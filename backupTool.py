#!/usr/bin/env python

from subprocess import PIPE, Popen, check_output
import re
import cStringIO

def giveNoUSBFindMsgAndQuit():
        print 'No USB Memory disk found'
        exit()

def safeUmountMedia():
    check_empty_out = check_output(["ls", "/media/pi"])
    # If empty then return
    if not len(check_empty_out):
        return
    # umount the media
    umount_out = check_output(["sudo", "umount", "/media/pi"])
    if len(umount_out):
        print "Error: " + umount_out

if __name__ == "__main__":
    # umount the media DIR before start
    safeUmountMedia()
    # Check if USB Memory disk attached
    blkid_out = check_output(["sudo", "blkid"])
    if len(blkid_out) == 0:
        giveNoUSBFindMsgAndQuit()
    
    # extract the useful info from the cmd output
    usb_list = re.split('\n', blkid_out)
    
    if len(usb_list) <= 0:
        giveNoUSBFindMsgAndQuit()
    
    print str(len(usb_list)) + " USB media found"    
    for item in usb_list:
        item_seg = re.split(': ', item)
        if len(item_seg) != 2:
            continue
        
        if "/dev/sda" not in item_seg[0]:
            continue
        usb_name = item_seg[0]
        print "USB memory disk found: " + usb_name
        item_rest_attrs = re.split(' ', item_seg[1])
        if len(item_rest_attrs) < 2:
            print usb_name + " is not a solid media"
            continue
        usb_type = ''
        usb_uuid = ''
        for attri in item_rest_attrs:
            attri_seg = re.split('="', attri)
            if len(attri_seg) != 2:
                print usb_name + "attribute error"
                break

            #print "Attribute: " + attri_seg[0] + attri_seg[1]
            if attri_seg[0] == 'UUID':
                usb_uuid = attri_seg[1][:-1]
            elif attri_seg[0] == 'TYPE':
                usb_type = attri_seg[1][:-1]
            
            if usb_uuid and usb_type:
                break
        # Check if data are acquired
        if not usb_uuid or not usb_type:
            print "No valid attributes found for " + usb_name
            continue
        # Mount the USB Memory disk
        mount_out = check_output(["sudo", "mount", "-t", usb_type, usb_name, "/media/pi"])
        if len(mount_out):
            print "error" + mount_out
            exit()
        # Check if it is a valid USB memory disk
        ls = Popen(('ls', '/media/pi'), stdout=PIPE)
        check_folder = check_output(('grep', '__laserAtlas__'), stdin=ls.stdout)
        ls.wait()
        if '__laserAtlas__' not in check_folder:
            print "No backup destination folder __laserAtlas__ found in USB"
            safeUmountMedia()
            continue
        
        # Copy the data files and log files
        copy_out = check_output(["sudo", "cp", "-R", "/home/pi/laser_atlas/data", "/media/pi/__laserAtlas__/"])
        if len(copy_out):
            print "Error: " + copy_out
            safeUmountMedia()
            exit()
        copy_out = check_output(["sudo", "cp", "-R", "/home/pi/laser_atlas/log", "/media/pi/__laserAtlas__/"])
        if len(copy_out):
            print "Error: " + copy_out
            safeUmountMedia()
            exit()
        # Umount the memory disk
        safeUmountMedia()
        print "Data and Log backup is done"
        break
    pass
    print "Exit"
    safeUmountMedia()
